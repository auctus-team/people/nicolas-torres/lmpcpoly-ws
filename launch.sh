#!/usr/bin/env bash
# set -ex
rosclean purge -y
# roslaunch panda_qp_control velocity_control.launch sim:=true
# roslaunch panda_qp_control torque_control.launch sim:=true load_gripper:=true
roslaunch panda_qp_control torque_control.launch sim:=true
