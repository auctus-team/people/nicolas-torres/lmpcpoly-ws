#!/usr/bin/env bash

correct_remote=auctus-team/people/nicolas-torres/lmpcpoly-ws.git
current_remote=$(git remote -v 2>&1)

shopt -s expand_aliases
alias trace_on='set -x'
alias trace_off='{ set +x; } 2>/dev/null'
alias say='{ set +x; } 2>/dev/null; echo $1'

correct_directory_msg(){
    echo "Error: incorrect directory, go to the repository dir of:"
    echo "${correct_remote}"
    echo "exiting..."
    exit 1
}

set -e

if [[ "${current_remote}" == *"not a git repository"* ]]; then
    correct_directory_msg
fi

if [[ "${current_remote}" == *"${correct_remote}"* ]]; then
    say "Correct directory"
else
    correct_directory_msg
fi

say "Continue"

source devel/setup.bash
# ./juggle.sh &
python src/lmpcpoly/lmpcpoly_moveit/test_pickplace.py
