#!/usr/bin/env python3

import subprocess
import sys
import os
import logging
import argparse

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))


def addLoggingLevel(levelName, levelNum, methodName=None):
    """
    source: https://stackoverflow.com/a/35804945
    Comprehensively adds a new logging level to the `logging` module and the
    currently configured logging class.

    `levelName` becomes an attribute of the `logging` module with the value
    `levelNum`. `methodName` becomes a convenience method for both `logging`
    itself and the class returned by `logging.getLoggerClass()` (usually just
    `logging.Logger`). If `methodName` is not specified, `levelName.lower()` is
    used.

    To avoid accidental clobberings of existing attributes, this method will
    raise an `AttributeError` if the level name is already an attribute of the
    `logging` module or if the method name is already present

    Example
    -------
    >>> addLoggingLevel('TRACE', logging.DEBUG - 5)
    >>> logging.getLogger(__name__).setLevel("TRACE")
    >>> logging.getLogger(__name__).trace('that worked')
    >>> logging.trace('so did this')
    >>> logging.TRACE
    5

    """
    if not methodName:
        methodName = levelName.lower()

    if hasattr(logging, levelName):
        raise AttributeError(
            '{} already defined in logging module'.format(levelName))
    if hasattr(logging, methodName):
        raise AttributeError(
            '{} already defined in logging module'.format(methodName))
    if hasattr(logging.getLoggerClass(), methodName):
        raise AttributeError(
            '{} already defined in logger class'.format(methodName))

    # This method was inspired by the answers to Stack Overflow post
    # http://stackoverflow.com/q/2183233/2988730, especially
    # http://stackoverflow.com/a/13638084/2988730
    def logForLevel(self, message, *args, **kwargs):
        if self.isEnabledFor(levelNum):
            self._log(levelNum, message, args, **kwargs)

    def logToRoot(message, *args, **kwargs):
        logging.log(levelNum, message, *args, **kwargs)

    logging.addLevelName(levelNum, levelName)
    setattr(logging, levelName, levelNum)
    setattr(logging.getLoggerClass(), methodName, logForLevel)
    setattr(logging, methodName, logToRoot)


addLoggingLevel('TRACE', logging.DEBUG - 5)
addLoggingLevel('NOTICE', logging.INFO + 5)

loglevel = logging.NOTICE


def call(cmd):
    logging.debug('+ {}'.format(cmd))

    def print_output(output, func):
        for line in output.splitlines():
            func(line)

    process = subprocess.Popen(cmd,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               shell=True,
                               universal_newlines=True)
    output, erroutput = process.communicate()

    process.wait()
    if process.returncode:
        print_output(output, logging.warning)
        print_output(erroutput, logging.error)
        logging.fatal(
            'Command \'{cmd}\' returned non-zero exit status {ret}.'.format(
                cmd=cmd, ret=process.returncode))
    else:
        print_output(output, logging.trace)
        print_output(erroutput, logging.warning)


parser = argparse.ArgumentParser()
parser.add_argument(
    '-v',
    '--verbosity',
    action='count',
    help='increase output verbosity, can be stacked (i.e: -vv)')
parser.add_argument(
    '--dev',
    action='store_true',
    help='setup ssh remotes for submodules (useful for developers)')
args = parser.parse_args()

logging.basicConfig(format='%(levelname)7s: %(message)s')
loglevel = logging.TRACE
if args.verbosity is not None:
    if args.verbosity > 2:
        loglevel = logging.TRACE
    elif args.verbosity > 1:
        loglevel = logging.DEBUG
    elif args.verbosity > 0:
        loglevel = logging.INFO
    if args.verbosity > 3:
        logging.warning('maxumum verbosity is -vvv')
logging.getLogger().setLevel(loglevel)
logging.notice('loglevel: {}, verbosity: {}'.format(
    logging.getLevelName(logging.root.level), args.verbosity))
logging.debug('loglevel: {}'.format(loglevel))
logging.debug('args: {}'.format(args))
logging.debug('abspath(__file__):'.format(srcabspath))

https_ssh = {
    'https://gitlab.inria.fr/': 'git@gitlab.inria.fr:',
    'https://github.com/': 'git@github.com:',
}
if not args.dev:
    logging.notice('--dev NOT option provided')
    https_ssh = {v: k for k, v in https_ssh.items()}
else:
    logging.notice('--dev option provided')

with open(os.path.join(srcabspath, '.gitmodules'), "r+") as f:
    flines = f.readlines()
    for kline, line in enumerate(flines):
        for key in https_ssh.keys():
            if key in line:
                repo = line.split('/')[-1].rstrip('.git\n')
                logging.debug('line {} (repo:{}): found {}'.format(
                    kline, repo, key))
                flines[kline] = line.replace(key, https_ssh[key])
with open(os.path.join(srcabspath, '.gitmodules'), "w") as f:
    for line in flines:
        f.write(line)

logging.notice('sync submodules')
call('git submodule sync')

logging.notice('update submodules, wait please...')
call('git submodule update --init --recursive')
