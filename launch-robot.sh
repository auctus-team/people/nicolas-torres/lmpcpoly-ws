#!/usr/bin/env bash
set -xe
rosclean purge -y
# roslaunch panda_qp_control velocity_control.launch robot_ip:=172.16.0.2
# roslaunch panda_qp_control velocity_control.launch robot_ip:=172.16.0.3 load_gripper:=true
roslaunch panda_qp_control torque_control.launch robot_ip:=172.16.0.3 load_gripper:=true
# roslaunch panda_qp_control torque_control.launch robot_ip:=172.16.0.3
