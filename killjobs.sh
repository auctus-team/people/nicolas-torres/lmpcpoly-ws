#!/usr/bin/env bash
jobbs=$(ps aux | grep -v grep | grep test_pickplace.py | awk '{print $2}')
echo "jobbs: ${jobbs}"
kill -9 ${jobbs}

# jobbs=$(ps aux | grep -v grep | grep juggler | awk '{print $2}')
# echo "jobbs: ${jobbs}"
# kill -9 ${jobbs}
