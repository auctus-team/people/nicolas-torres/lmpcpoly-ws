# lmpcpoly_ws

A workspace to test lmpcpoly

# Quickstart

```bash
git clone https://gitlab.inria.fr/auctus-team/people/nicolas-torres/lmpcpoly-ws.git
cd lmpcpoly_ws
./setupws.py # add --dev if you wish to develop
./build.sh
./launch.sh # launches simulation, for robot use launch-robot.sh
```

# Compiling from source

**Quickstart**

```bash
# assuming inside docker env or dependencies installed
git clone https://gitlab.inria.fr/auctus-team/people/nicolas-torres/lmpcpoly-ws.git
cd lmpcpoly_ws
./setupws.py -vvv # -vvv adds more verbosity to see what it's doing, but it's optional
./build-launch.sh # runs ./build.sh and then ./launch.sh
```

**Developers**: if you wish to change the urls to use ssh/git instead of https, use instead:
```bash
./setupws.py -vvv --dev
```

**Catkin workspace**

`./launch.sh` (executed by `./build-launch.sh`) runs `source devel/setup.bash` before executing a `roslaunch` command.

If you don't want to use these scripts, you may want to add `source /absolute/path/to/devel/setup.bash` (using the absolute path) to your `~/.bashrc` to avoid having to source it every time you open a new terminal.

## Dependencies

We are assuming that ROS noetic is already installed on your computer.

Ubuntu packages (NOT needed in the docker environment):
```bash
sudo apt install python3-rosdep python3-catkin-tools build-essential cmake git libpoco-dev libeigen3-dev
```

# Using the docker dev environment

Inside the directory `docker_env`, you will find these scripts:

- `docker_env/build.sh`: builds the docker image "lmpcpoly"
- use [dogi](https://github.com/ntorresalberto/dogi) to run the docker image

**Quickstart**

To create the docker image and use it:

```bash
cd docker_env
./build.sh        # builds docker environment
cd ../            # go to workspace root
dogi run lmpcpoly # launches a terminal inside the environment
```

Once in the container, follow #compiling-from-source-steps

# Usage

## In simulation 

After compiling and sourcing the `setup.bash` (see previous steps), you can test the **velocity controller** and **torque controller** with one of these:
```
roslaunch panda_qp_control velocity_control.launch sim:=true
roslaunch panda_qp_control torque_control.launch sim:=true
```

**NOTE**: The simulation of the robot in velocity is a bit hacky for now. It uses a EffortInterface. The joint velocity computed by the QP is fed to a proportionnal controller as $`k_p (\dot{q}^{opt} - \dot{q})`$. This is because for now the [franka_gazebo](https://frankaemika.github.io/docs/franka_ros.html#franka-gazebo) doesn't take into consideration a VelocityJointInterface. Hopefully this will be fixed with [this PR](https://github.com/frankaemika/franka_ros/pull/181).


## On the real robot

For a **velocity controller** and **torque controller** use one of these:
```
roslaunch velocity_qp velocity_control.launch robot_ip:=your_robot_ip
roslaunch panda_qp_control torque_control.launch robot_ip:=your_robot_ip
```

**NOTE**: There is the same error as with the simulation. Therefore the robot is not stiffly actuated. This can be fixed by using a hardware_interface::VelocityJointInterface but it prevents simulating the robot.

## Run a trajectory

In this version of panda_qp_control, trajectories are computed using MoveIt. Several examples of trajectories are available in the package [moveit_trajectory_interface/test](https://gitlab.inria.fr/auctus-team/components/motion-planning/moveit_trajectory_interface/-/tree/master/test). 

To launch a trajectory simply run the command :
```
rosrun moveit_trajectory_interface go_to_cartesian_pose.py
```

[MoveIt](https://moveit.ros.org/) is used to allow a more high level definition of a trajectory than with [panda_traj](https://gitlab.inria.fr/auctus-team/components/control/panda_traj). MoveIt also include obstacle avoidance feature when planning for a motion. External sensors can also be added to sample the environment and take it into account durint the planning. The [moveit_trajectory_interface](https://gitlab.inria.fr/auctus-team/components/motion-planning/moveit_trajectory_interface) is a package that grabs a trajectory object planned by moveit and exposes the resulting trajectory as a function of time. A new point along the trajectory must be fed to the QP controller at each time step (1ms). More information on the trajectory generation can be found [here](https://gitlab.inria.fr/auctus-team/components/motion-planning/moveit_trajectory_interface))

If one wants to use the old way to generate trajectories, a branch with kdl_trajectories is available [here](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_control/-/tree/kdl_trajectories).

## Optional roslaunch parameters: 

- `robot_ip`: IP adress of the panda robot

- `sim`: Run the code in simulation on Gazebo

- `load_gripper`: launch the panda model with its gripper

## Controller parameters

The controller parameters are stored in a yaml file in the `/config` folder and loaded as ros parameters in the `run.launch` file. 

