#!/usr/bin/env sh
set -xe

docker image rm -f auctus
docker image prune -f
