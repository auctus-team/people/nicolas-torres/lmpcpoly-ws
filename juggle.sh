#!/usr/bin/env bash

correct_remote=auctus-team/components/robots/panda/panda_qp_ws.git
current_remote=$(git remote -v 2>&1)

shopt -s expand_aliases
alias trace_on='set -x'
alias trace_off='{ set +x; } 2>/dev/null'
alias say='{ set +x; } 2>/dev/null; echo $1'

correct_directory_msg(){
    echo "Error: incorrect directory, go to the repository dir of:"
    echo "${correct_remote}"
    echo "exiting..."
    exit 1
}

set -e

if [[ "${current_remote}" == *"not a git repository"* ]]; then
    correct_directory_msg
fi

if [[ "${current_remote}" == *"${correct_remote}"* ]]; then
    say "Correct directory"
else
    correct_directory_msg
fi

say "Continue"

source devel/setup.bash
#rosrun plotjuggler plotjuggler --layout src/panda_qp_control/config/plotjuggler_velocity_limits_separated.xml
# rosrun plotjuggler plotjuggler --layout src/panda_qp_control/config/plotjuggler_velocity_showcase.xml
# rosrun plotjuggler plotjuggler --layout src/panda_qp_control/config/plotjuggler_velocity_new.xml
# rosrun plotjuggler plotjuggler --layout rviz_obstacle.xml
# rosrun plotjuggler plotjuggler --layout src/panda_qp_control/config/plotjuggler_velocity_tracker.xml
# rosrun plotjuggler plotjuggler --layout src/panda_qp_control/config/plotjuggler_velocity_tracker_acc.xml
rosrun plotjuggler plotjuggler --layout src/panda_qp_control/config/plotjuggler_velocity_tracker_acc_limits.xml
# rosrun plotjuggler plotjuggler --layout src/panda_qp_control/config/plotjuggler_velocity_tracker_jerk.xml
# rosrun plotjuggler plotjuggler
