#!/usr/bin/env bash
export USER=$(id -un)
correct_remote=auctus-team/people/nicolas-torres/lmpcpoly-ws.git
current_remote=$(git remote -v 2>&1)

shopt -s expand_aliases
alias trace_on='set -x'
alias trace_off='{ set +x; } 2>/dev/null'
alias say='{ set +x; } 2>/dev/null; echo $1'

correct_directory_msg(){
    echo "Error: incorrect directory, go to the repository dir of:"
    echo "${correct_remote}"
    echo "exiting..."
    exit 1
}

set -e

if [[ "${current_remote}" == *"not a git repository"* ]]; then
    correct_directory_msg
fi

if [[ "${current_remote}" == *"${correct_remote}"* ]]; then
    say "Correct directory"
else
    correct_directory_msg
fi

say "Continue"

# http://wiki.ros.org/ROS/EnvironmentVariables#ROS_LANG_DISABLE
# export ROS_LANG_DISABLE=genlisp:roslisp:gennodejs:rosnodejs:geneus:roseus
export ROS_LANG_DISABLE=genlisp:gennodejs:geneus

trace_on
find . | ag conflicted | xargs -i{} rm -v '{}'
trace_off

DIR1=build/
DIR2=build_isolated/
if [ -d "$DIR1" ] || [ -d "$DIR2" ];
then
    echo "$DIR1 and/or $DIR2 directory exists. Skipping first time steps."
else
	  echo "$DIR1 and/or $DIR2 does not exist. Doing first time steps."
    trace_on
    sudo apt update && sudo apt upgrade -y
    sudo -HE pip install pycapacity
    rosdep update --rosdistro=noetic
    rosdep install --from-paths src --ignore-src -r -y
    pushd src/ruckig
    test -e "pybind11" || git clone https://github.com/pybind/pybind11.git
    popd
    trace_off
fi
trace_on

# catkin clean -y || true
# rm -rf .catkin_tools/
# catkin config --init --extend /opt/ros/noetic --cmake-args -DCMAKE_BUILD_TYPE=Release
# catkin config --init --extend /opt/ros/noetic --cmake-args -DPYTHON_EXECUTABLE=/usr/bin/python3
# catkin config --init --extend /opt/ros/noetic --cmake-args -Dosqp_INCLUDE_DIRS=$(pwd)/devel/include
# NOTE -DBUILD_PYTHON_MODULE=ON is used by ruckig
# BUILD_TYPE=Release
BUILD_TYPE=RelWithDebInfo
catkin config --init --extend /opt/ros/noetic \
          --cmake-args \
          -DPYTHON_EXECUTABLE=/usr/bin/python3 \
          -DBUILD_PYTHON_MODULE=ON \
          -DBUILD_WITH_VECTORIZATION_SUPPORT=OFF \
          -DCMAKE_BUILD_TYPE=${BUILD_TYPE}
catkin build -j7 --make-args -j7
rm -vf ~/.ros/lmpc*.csv
trace_off

export ROSCONSOLE_FORMAT='[${severity}] [${logger}][${function}.line${line}]: ${message}'

echo ""
echo "----------------------------------------------------"
echo "----------------------------------------------------"
echo ""
echo "Now you can execute:"
echo "source devel/setup.bash"
echo "./launch.sh"
echo "./launch-robot.sh"
